console.log('server start');

global.glob = {};

glob.modules = {
  http: require('http'),
  url: require('url'),
  fs: require('fs'),
  child_process: require('child_process'),
  util: require('util'),
  mongoose: require('mongoose'),
  express: require('express'),
  winston: require('winston')
};

module.exports.start = function (port,env,cb) {
  port = port || 3100;
  env = env || 'development';
  glob.config = require('../config')(env);
  glob.utils = require('./utils');
  glob.spawn = glob.modules.child_process.spawn
  glob.modules.winston.addColors({
    info: 'blue',
    warn: 'yellow',
    error: 'red'
  });

  global.log = new glob.modules.winston.Logger({
    transports: [
      new glob.modules.winston.transports.Console({
        level: glob.config.log.level,
        colorize: 'true'
      }), new glob.modules.winston.transports.File({
        filename: glob.config.log.path
      })
    ],
    exitOnError: false
  });

  global.mongo = require('./mongo');

  var express = glob.modules.express
  var app = express();  

  app.configure(function() {
      app.set('views', __dirname + '/views');
      app.set('env',env);
      app.use(express.cookieParser());
      app.use(express.session({
        secret: glob.config.app.secretString,
        cookie: {
          maxAge: glob.config.app.maxAge
        }
      }));
      app.use(express.bodyParser());
      app.use(express.methodOverride());
      app.set('view engine', 'jade');
      app.use(app.router);
      app.use(express["static"](__dirname+'/public'));
  });

  app.configure("development", function() {
    app.use(express.errorHandler({
      dumpExceptions: true,
      showStack: true
    }));
  });

  app.configure("production", function() {
    app.use(express.errorHandler({
      dumpExceptions: true,
      showStack: true
    }));
  });


  require('./router')(app);
  glob.modules.mongoose.connect(glob.config.db.mongoUrl, function(err) {
    if (err) {
      console.log('mongo err:');
      console.log(err);
    }
    glob.modules.http.createServer(app).listen(port, function() {
        log.info('Server start on port ' + port + ' in ' + app.settings.env + ' mode');
        if (cb) { cb() };
    });
  });
};
