var logFrom = 'router.scrapes ';
var childProcess = require('child_process');
var fs = require('fs');
var path = require('path');

// startup function for scrapers
var all = function(req, res) {
    mongo.bill.find({}, function(err, bills) {
      if (!err) {
        if (bills && bills.length > 0) {
          var errorsObj = {};
          errorsObj.stderr = [];  
          errorsObj.error = [];  

          var counter = 0;

          for (var i = 0, bill = {}; i < bills.length; i++) {
            bill = bills[i];
            if(bill.login && bill.password) {
              // collect user data in process.env
              process.env.userLogin = bill.login;
              process.env.userPass = bill.password;
              process.env.billId = bill.entity.id;
              process.env.userId = bill.userId;
              process.env.scrapesUrl = glob.config.scrapes.saveUrl;
              // choose a path to a specific scrape
              var scrapePath =  path.resolve(__dirname, "../scrapes/", bill.supplier+".js");
              if (fs.existsSync(scrapePath)) {
                  // run casperjs with childProcess.exec(), because casperjs is not an npm module.
                  var args = "casperjs " + __dirname+"/../scrapes/" + bill.supplier + ".js " + "--web-security=no";
                  var casperjs = childProcess.exec(args, {
                      stdio: 'inherit',
                      stderr: 'inherit',
                      env: process.env,
                      timeout: glob.config.scrapes.timeOut
                  }, function (error, stdout, stderr) {

                      console.log('stdout: ' + stdout);
                      console.log('error: ' + error);
                      console.log('stderr: ' + stderr);
                      counter++;

                      if (stderr) {
                        errorsObj.stderr[i] = stderr;
                      }
                      if (error) {
                        errorsObj.error[i] = error;
                      }
                      
                        var notifyUserId = [bill.userId];
                        if (stderr && stderr != '') {
                            // add notification if error, commented because scrape interval is very low setup
                            // mongo.notification.create({sharedUsersId: notifyUserId, dateNotify: Date.now(), message: 'Scraping error with '+bill.supplier+'. Please double check to be sure your account details' }, function(err, notification) {
                            //     if(!err){
                            //         if(notification){
                            //           log.info(logFrom, 'notification created', notification);
                            //         }else{
                            //           log.error(logFrom, 'notification not created')
                            //         }
                            //     }else{
                            //       log.error(logFrom, err);
                            //     }
                            // })

                            res.send({
                              code: 400,
                              error: errorsObj.stderr
                            });
                        }
                        if (stdout && error == null) {
                          // console.log('stdout: ' + stdout);
                          log.info(logFrom, 'scrape finished success');
                          res.send ({
                            code: 200,
                            data: 'scrapes success'
                          });
                        } else if(stdout == '' && error == null) {
                          // add notification if error, commented because scrape interval is very low setup
                          // mongo.notification.create({sharedUsersId: notifyUserId, dateNotify: Date.now(), message: 'Scraping error with '+bill.supplier+'. Please double check to be sure your account details' }, function(err, notification) {
                          //       if(!err){
                          //           if(notification){
                          //             log.info(logFrom, 'notification created', notification);
                          //           }else{
                          //             log.error(logFrom, 'notification not created')
                          //           }
                          //       }else{
                          //         log.error(logFrom, err);
                          //       }
                          //   })

                            res.send({
                              code: 400,
                              error: 'Scraping error'
                            });
                        }
                        if(error != null) {
                          // add notification if error, commented because scrape interval is very low setup
                          // mongo.notification.create({sharedUsersId: notifyUserId, dateNotify: Date.now(), message: 'Scraping error with '+bill.supplier+'. Please double check to be sure your account details' }, function(err, notification) {
                          //     if(!err){
                          //         if(notification){
                          //           log.info(logFrom, 'notification created', notification);
                          //         }else{
                          //           log.error(logFrom, 'notification not created')
                          //         }
                          //     }else{
                          //       log.error(logFrom, err);
                          //     }
                          // })

                          res.send({
                            code: 400,
                            error: errorsObj.error
                          });
                        }
                  });

                  log.info(logFrom, bill.supplier+' scrape file for this suplier start success');
                  console.log('Spawned child pid: ' + casperjs.pid);

                  casperjs.on('exit', function (code) {
                      console.log('child process exited with code ' + code);
                      // casperjs.kill('SIGHUP');
                  });
              } else { 
                  log.warn(logFrom, bill.supplier+' scrape file for this suplier does not exist');
              }
            } else {
              log.warn(logFrom, 'login or password not found');
              res.send({
                code: 400,
                error: 'login or password not found'
              });
            }
          }
        } else {
          log.warn(logFrom, 'bill not found');
          res.send({
            code: 400,
            error: 'bill not found'
          });
        }
      } else {
        log.error(logFrom, 'input data error');
        res.send ({
          code: 400,
          error: 'input params not found'
        });
      }
    });
  }

module.exports = function (app) {
    app.get('/scrapeAll', all);
}; 
