var utils = {};

utils.crypt = function(str) {
  var cipher, crypted;
  cipher = glob.modules.crypto.createCipher('aes-256-cbc', glob.config.app.secretString);
  crypted = cipher.update(str, 'utf8', 'hex');
  return crypted += cipher.final('hex');
};

utils.decrypt = function(str) {
  var decipher, decrypted;
  try {
    decipher = glob.modules.crypto.createDecipher('aes-256-cbc', glob.config.app.secretString);
    decrypted = decipher.update(str, 'hex', 'utf8');
    return decrypted += decipher.final('utf8');
  } catch (err) {
    log.warn(err, 'utils.decrypt');
    return false;
  }
};

utils.randomString = function(length) {
  var chars, i, str;
  chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz".split("");
  if (!length) {
    length = Math.floor(Math.random() * chars.length);
  }
  str = "";
  i = 0;
  while (i < length) {
    str += chars[Math.floor(Math.random() * chars.length)];
    i++;
  }
  return str;
};

utils.generateNumericString = function(length) {
  var chars, i, str;
  chars = "0123456789".split("");
  if (!length) {
    length = Math.floor(Math.random() * chars.length);
  }
  str = "";
  i = 0;
  while (i < length) {
    str += chars[Math.floor(Math.random() * chars.length)];
    i++;
  }
  return str;
};

utils.generateToken = function () {
  var chars = "_!abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
      token = utils.randomString(6)+new Date().getTime();
  for ( var x = 0; x < 16; x++ ) {
    var i = Math.floor( Math.random() * 62 );
    token += chars.charAt( i );
  }
  return token;
}; 

module.exports = utils;
