function arrayToURL(array) {
  var pairs = [];
  for (var key in array)
    if (array.hasOwnProperty(key))
      pairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(array[key]));
  return pairs.join('&');
}

var casper = require('casper').create({
    pageSettings: {
         loadImages:  false,
         userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    },
    verbose: true, 
    logLevel: 'debug'
});

var system = require('system');
var data = [];
if(system.env.userLogin && system.env.userPass && system.env.billId && system.env.userId && system.env.scrapesUrl) {
    var scrapesUrl = system.env.scrapesUrl;
    var userLogin = system.env.userLogin;
    var userPass = system.env.userPass;
    var billId = system.env.billId;
    var userId = system.env.userId;
    try {
        casper.start('https://www.npower.com/At_home/Applications/Npower.Web.Login/login.aspx').then(function() {
            console.log("page loaded");
            this.fill('form', { Username: userLogin, Password: userPass }, true);
        });

        casper.thenOpen('https://www.npower.com/at_home/applications/atlas.web/account.aspx', function() {
            // get all html data in table
            var el = this.getHTML('.no_padding');

            data["el"] = el;
            data["billId"] = billId;
            data["userId"] = userId;
            var urlQuery = arrayToURL(data);
            var server = scrapesUrl+'/parse/?'+urlQuery;
            var page = require('webpage').create();
            // send data to parse server page
            page.open(server, 'post', function (status) {
                if (status !== 'success') {
                    this.echo('Unable to post!');
                } else {
                    this.echo('post success');
                }
            });
        });

        casper.run(function() {
            this.exit();
        });
    } catch (e) {
        console.log(e)
    }
} else {
    console.log('input data not defined');
}

