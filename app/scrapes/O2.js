function arrayToURL(array) {
  var pairs = [];
  for (var key in array)
    if (array.hasOwnProperty(key))
      pairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(array[key]));
  return pairs.join('&');
}

// casperjs initialization
var casper = require('casper').create({
    pageSettings: {
         loadImages:  false,
         userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    },
    verbose: true, 
    logLevel: 'debug'
});

var system = require('system');
var data = [];
// check input data
if(system.env.userLogin && system.env.userPass && system.env.billId && system.env.userId && system.env.scrapesUrl) {
    var scrapesUrl = system.env.scrapesUrl;
    var userLogin = system.env.userLogin;
    var userPass = system.env.userPass;
    var billId = system.env.billId;
    var userId = system.env.userId;
    try {
        // go to login page
        casper.start('https://accounts.o2.co.uk/signin').then(function() {
            console.log("page loaded");
            this.echo("Current url " + this.getCurrentUrl());
            // log in
            this.fill('form[name="loginForm"]', { username: userLogin, password: userPass }, true);
        });
        // go to first iframe widget (first part of data), it is because the location of information on the page in iframes
        casper.thenOpen('https://mymobile.o2.co.uk/paymonthly/mybill/summary-widget', function() {
            this.echo(this.getCurrentUrl());
            // this.debugPage();
            // collect all needed ids
            var pageElements = [];
            pageElements.push("#balance");
            pageElements.push("#nextbillDate");
            pageElements.push("#billDate");
            pageElements.push("#paymentDueDate");
            pageElements.push("#latestBillAmount");

            for (var i = 0; i < pageElements.length; i++) {
                if (this.exists(pageElements[i])) {
                    var element = '';
                    // get data from elements
                    element = this.fetchText(pageElements[i]).trim();
                    if(element.indexOf('£') !== -1) {
                        element = element.replace('£', '');
                        element = parseFloat(element);
                    }
                    data[pageElements[i].substr(1)] = element;
                }
            };

            data["billId"] = billId;
            data["userId"] = userId;

            var urlQuery = arrayToURL(data);
            var server = scrapesUrl+'/savescrapes/?'+urlQuery;
            var page = require('webpage').create();
            // send first part of data to save scrapes server
            page.open(server, 'post', function (status) {
                if (status !== 'success') {
                    console.log('Unable to post!');
                } else {
                    console.log('post success');
                }
            });
        });
        // go to next iframe widget (second part of data)
        casper.thenOpen('https://mymobile.o2.co.uk/paymonthly/mytariffandboltons/tariffsummary-widget', function() {
            this.echo(this.getCurrentUrl());
            // this.debugPage();
            var pageElements = [];
            pageElements.push("#lengthOfContractValue");
            pageElements.push("#contractRenewalDateValue");
            pageElements.push("#monthlyChargeValue");
            pageElements.push("#tariffNameValue");

            for (var i = 0; i < pageElements.length; i++) {
                console.log(i)
                console.log(this.exists(pageElements[i]))
                if (this.exists(pageElements[i])) {
                    var element = '';
                    element = this.fetchText(pageElements[i]).trim();
                    if(element.indexOf('£') !== -1) {
                        element = element.replace('£', '');
                        element = parseFloat(element);
                    }
                    data[pageElements[i].substr(1)] = element;
                    console.log(typeof(this.fetchText(pageElements[i])))
                    console.log(this.fetchText(pageElements[i]))
                }
            };

            data["billId"] = billId;
            data["userId"] = userId;

            var urlQuery = arrayToURL(data);
            var server = scrapesUrl+'/updatescrapes/?'+urlQuery;
            var page = require('webpage').create();
            // send second part of data to save scrapes server in update script
            page.open(server, 'post', function (status) {
                if (status !== 'success') {
                    console.log('Unable to post!');
                } else {
                    console.log('post success');
                }
            });
        });
        // go to last iframe widget
        casper.thenOpen('https://mymobile.o2.co.uk/paymonthly/recentcharges-widget', function() {
            this.echo(this.getCurrentUrl());
            // this.debugPage();

            // select elements with class cost
            var element = this.fetchText(".cost");
            // copy last of them
            sum = "";
            for(i=element.length-1; i>0 ;i--){
                if(element[i] != "£"){
                    sum = element[i]+sum
                }else{
                    break
                }
            }
            element = sum;

            data["billCredit"] = element;
            data["billId"] = billId;
            data["userId"] = userId;

            var urlQuery = arrayToURL(data);
            var server = scrapesUrl+'/updatescrapes/?'+urlQuery;
            var page = require('webpage').create();
            // send second part of data to save scrapes server in update script
            page.open(server, 'post', function (status) {
                if (status !== 'success') {
                    console.log('Unable to post!');
                } else {
                    console.log('post success');
                }
            });
        });

        casper.run(function() {
            this.echo(this.getCurrentUrl());
            this.exit();
        });
    } catch (e) {
        console.log(e);
    }
} else {
    console.log('input data not defined');
}