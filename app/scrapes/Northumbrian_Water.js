function arrayToURL(array) {
  var pairs = [];
  for (var key in array)
    if (array.hasOwnProperty(key))
      pairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(array[key]));
  return pairs.join('&');
}

var casper = require('casper').create({
    pageSettings: {
         loadImages:  false,
         userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    },
    verbose: true, 
    logLevel: 'debug'
});

var system = require('system');
var data = [];
if(system.env.userLogin && system.env.userPass && system.env.billId && system.env.userId && system.env.scrapesUrl) {
    var scrapesUrl = system.env.scrapesUrl;
    var userLogin = system.env.userLogin;
    var userPass = system.env.userPass;
    var billId = system.env.billId;
    var userId = system.env.userId;
    try {
        casper.start('https://www.nwl.co.uk/your-home/your-account.aspx?').then(function() {
            console.log("page loaded");
            this.echo("Current url " + this.getCurrentUrl());
            // this.fill('form[name="loginForm"]', { username: userLogin, password: userPass }, true);
        });

        casper.thenOpen('https://www.nwl.co.uk/your-home/your-account.aspx?', function() {
            this.echo(this.getCurrentUrl());
            // console.log(this.getElementAttribute('#nav_mybill a', 'href'));
            // this.getElementAttribute('#nav_mybill a', 'href');
        });

        casper.run(function() {
            // this.debugPage();
            this.echo(this.getCurrentUrl());
            this.exit();
        });
    } catch (e) {
        console.log(e)
    }
} else {
    console.log('input data not defined');
}

