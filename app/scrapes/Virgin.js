function arrayToURL(array) {
  var pairs = [];
  for (var key in array)
    if (array.hasOwnProperty(key))
      pairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(array[key]));
  return pairs.join('&');
}

var casper = require('casper').create({
    pageSettings: {
         loadImages:  false,
         userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    },
    verbose: true, 
    logLevel: 'debug'
});

var system = require('system');
var data = [];
if(system.env.userLogin && system.env.userPass && system.env.billId && system.env.userId && system.env.scrapesUrl) {
    var scrapesUrl = system.env.scrapesUrl;
    var userLogin = system.env.userLogin;
    var userPass = system.env.userPass;
    var billId = system.env.billId;
    var userId = system.env.userId;
    try {
        casper.start('http://my.virginmedia.com/home/signIn').then(function() {
            this.fill('form#login', { username: userLogin, password: userPass }, true);
        });
        casper.thenOpen('https://my.virginmedia.com/my-bills/index', function() {
            // console.log(this.exists('p.amount'))
            // var element = this.fetchText('p.amount').trim();
            // var element2 = this.fetchText('p.date > span').trim();
            // var element3 = this.fetchText('p.date > strong').trim();
            // var pageElements = [];
            // pageElements.push("#balance");
            // pageElements.push("#nextbillDate");
            // pageElements.push("#billDate");

            // for (var i = 0; i < pageElements.length; i++) {
            //     console.log(i)
            //     console.log(this.exists(pageElements[i]))
            //     if (this.exists(pageElements[i])) {
            //         var element = '';
            //         element = this.fetchText(pageElements[i]).trim();
            //         if(element.indexOf('£') !== -1) {
            //             element = element.replace('£', '');
            //             element = parseFloat(element);
            //         }
            //         data[pageElements[i].substr(1)] = element;
            //         console.log(typeof(this.fetchText(pageElements[i])))
            //         console.log(this.fetchText(pageElements[i]))
            //     }
            // };

            // data["billId"] = billId;
            // data["userId"] = userId;

            // var urlQuery = arrayToURL(data);
            // var server = scrapesUrl+'/savescrapes/?'+urlQuery;
            // var page = require('webpage').create();

            // page.open(server, 'post', function (status) {
            //     if (status !== 'success') {
            //         console.log('Unable to post!');
            //     } else {
            //         console.log('post success');
            //     }
            // });
        });

        casper.run(function() {
            this.exit();
        });
    } catch (e) {
        console.log(e)
    }
} else {
    console.log('input data not defined');
}

