var mongoose = glob.modules.mongoose;

var notification = new mongoose.Schema({
  billId:               { type: "string" },
  sharedUsersId:        { type: "array", items: [ { type: "string" } ] },
  message:              { type: "string" },
  viewed:               { type: "bool", default: false },
  dateNotify:           { type: "date" },
  dateViewed:           { type: "date" }
});

notification.virtual('entity').get(function () {
  var entity = {
    id: this._id.toString(),
    entity: 'notification',
    userId: this.userId,
    billId: this.billId,
    sharedUsersId: this.sharedUsersId,
    message: this.message,
    viewed: this.viewed,
    dateNotify: this.dateNotify,
    dateViewed: this.dateViewed
  };
  return entity;
});

module.exports = mongoose.model('notification', notification);
