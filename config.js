var url = require('url');
module.exports = function (env) {
  var config;
  config = {
      app: {
          url: 'http://localhost:3100',
          name: 'Scrapes',
          port: 3100,
          secretString: 'secretString',
          maxAge: 3*60*60*1000
      },
      log: {
          level: 'info',
          path: __dirname+'/server.log'
      },
      db: {
          mongoUrl: 'mongodb://heroku:71eb5116ad068bd32aa9fab0b215faa9@alex.mongohq.com:10062/app10352788', // for heroku
          // mongoUrl: process.env.MONGOHQ_URL || 'mongodb://localhost/household', //localhost
      },
      scrapes: {
        saveUrl: 'http://getscrapes.herokuapp.com', // for heroku
        // saveUrl: 'http://localhost:5000', //localhost
        timeOut: 20*1000
      }
  };
  switch (env) {
    case 'production': {
      config.app.url = 'http://scrapes.herokuapp.com'; // for heroku
      // config.app.url = 'http://localhost:3100'; //localhost
      config.db.mongoUrl = 'mongodb://heroku:71eb5116ad068bd32aa9fab0b215faa9@alex.mongohq.com:10062/app10352788'; // for heroku
      // config.db.mongoUrl = process.env.MONGOHQ_URL || 'mongodb://localhost/household'; //localhost
      config.scrapes.saveUrl = 'http://getscrapes.herokuapp.com'; // for heroku
      // config.scrapes.saveUrl = 'http://localhost:5000'; //localhost
      break;
    }
    case 'development': {
      config.db.mongoUrl = 'mongodb://heroku:71eb5116ad068bd32aa9fab0b215faa9@alex.mongohq.com:10062/app10352788'; // for heroku
      // config.db.mongoUrl = process.env.MONGOHQ_URL || 'mongodb://localhost/household'; //localhost
      break;
    }
    default: {
      break;
    }
  }

  return config;
};

